﻿using System;
using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;

[System.Serializable]
public class TankManager
{
    //All Variables
        public Color playerColor;
        public Transform spawnPoint;
        [HideInInspector] public int playerNumber;
        [HideInInspector] public string coloredPlayerText;
        [HideInInspector] public GameObject Instance;
        [HideInInspector] public int win;
         
        private TankMovement movement;
        private TankShooting shooting;
        private GameObject canvas;
        
    //Set Up for Tank in the GameManager with player number and color to choose
        public void Setup()
        {
            movement = Instance.GetComponent<TankMovement>();
            shooting = Instance.GetComponent<TankShooting>();
            canvas = Instance.GetComponentInChildren<Canvas>().gameObject;
            movement.players = playerNumber;
            shooting.players = playerNumber;
            coloredPlayerText = "<color=#" + ColorUtility.ToHtmlStringRGB(playerColor) + ">PLAYER" + playerNumber + "</color>";
            MeshRenderer[] renderers = Instance.GetComponentsInChildren<MeshRenderer>();
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].material.color = playerColor;
            }
        }
        
    //Disable control for our tank
        public void DisableControl()
        {
            movement.enabled = false;
            shooting.enabled = false;
            canvas.SetActive(false);
        }

        //Enable control for our tank
        public void EnableControl()
        {
            movement.enabled = true;
            shooting.enabled = true;
            canvas.SetActive(true);
        }
        
        //Reset tanks to their positions
        public void Reset()
        {
            Instance.transform.position = spawnPoint.position;
            Instance.transform.rotation = spawnPoint.rotation;
            Instance.SetActive(false);
            Instance.SetActive(true);
        }
    }
