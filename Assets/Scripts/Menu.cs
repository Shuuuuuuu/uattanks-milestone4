﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    //Start the music in Menu
   private void Start()
    {
        AudioSource startMusic = GetComponent<AudioSource>();
        startMusic.Play();
    }

    //When you push PVP Load the Main
    public void PVP()
    {
        SceneManager.LoadScene("Main");
    }

    //When you push PVE Load the PVE
    public void PVE()
    {
        SceneManager.LoadScene("PvE");
    }

    public void Options()
    {
        SceneManager.LoadScene("Options");
    }

    //Go Back to Menu
    public void Back()
    {
        SceneManager.LoadScene("Menu");
    }

    //Push Quit Exits the game
    public void Quit()
    {
        Application.Quit();
        Debug.Log("Application Quit");
    }
}
