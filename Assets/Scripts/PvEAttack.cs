﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PvEAttack : MonoBehaviour
{
    //All Variables
    public GameObject shellPrefab;
    public float shellSpeed = 10;
    public AudioClip shootingAudio;
    private Transform fireposition;
    private AudioSource fireAudio;

    // Start is called before the first frame update
    void Start()
    {
        //Find the FireStart GameObject
        fireposition = transform.Find("FireStart");
    }

    //When Shooting it will Instantiate a shell clone
   public void Shoot()
    {
        fireAudio = this.GetComponent<AudioSource>();
        fireAudio.volume = 1.0f;
        fireAudio.Play();
        AudioSource.PlayClipAtPoint(shootingAudio, transform.position);
        GameObject go = GameObject.Instantiate(shellPrefab, fireposition.position, fireposition.rotation) as GameObject;
        go.GetComponent<Rigidbody>().velocity = go.transform.forward * shellSpeed;
    }
}
