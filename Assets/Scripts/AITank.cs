﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AITank : MonoBehaviour
{
    //Variables Used
    public float moveSpeed;
    public float attackRange;
    public float shootCoolDown;
    private float timer;
    private PvEAttack pveAttack;

    void Start()
    {
        //Get the component from PVEAttack script
        pveAttack = GetComponent<PvEAttack>();
    }

   public void FixedUpdate()
    {
        //Find our player
        GameObject player = GameObject.FindWithTag("Player");
        //if there is no player make it unavailable
        if (player == null) return;
        //TankAi movement
        timer += Time.fixedDeltaTime;
        float distance = Vector3.Distance(player.transform.position, transform.position);
        //AttackRange of AI
        if(distance > attackRange)
        {
            transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
        }

        transform.LookAt(player.transform.position);

        //CoolDown for Shooting
        if(timer > shootCoolDown)
        {
            pveAttack.Shoot();
                timer = 0f;
        }
    }
}
